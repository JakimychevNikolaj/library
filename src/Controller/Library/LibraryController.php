<?php

namespace App\Controller\Library;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Category;
use App\Form\BookType;
use App\Repository\AuthorRepository;
use App\Repository\BookRepository;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\String\Slugger\SluggerInterface;

class LibraryController extends AbstractController
{
    private BookRepository $bookRepository;
    private AuthorRepository $articleRepository;
    private CategoryRepository $categoryRepository;
    private SluggerInterface $slugger;

    public function __construct(
        BookRepository $bookRepository,
        AuthorRepository $articleRepository,
        CategoryRepository $categoryRepository,
        SluggerInterface $slugger
    )
    {
        $this->bookRepository = $bookRepository;
        $this->articleRepository = $articleRepository;
        $this->categoryRepository = $categoryRepository;
        $this->slugger = $slugger;
    }

    /**
     * @Route("/", name="app_library")
     */
    public function index(): Response
    {
        return $this->render('library/index.html.twig', [
            'data' => $this->bookRepository->findAll(),
        ]);
    }

    /**
     * @Route("/create", name="book_create")
     */
    public function create(Request $request): Response
    {
        $book = new Book();
        $authors = $this->articleRepository->getAuthorsForSelect();
        $categories = $this->categoryRepository->getCategoriesForSelect();

        $form = $this->createForm(
            BookType::class,
            $book,
            ['authors' => $authors, 'categories' => $categories]
        );
        $form->handleRequest($request);
        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('library/create.html.twig', [
                'form' => $form->createView(),
            ]);
        }

        $cover = $form->get('cover')->getData();
        $this->uploadFile($cover, $book, $request);
        if ($categoryIds = $form->get('categories')->getData()) {
            $book->setCategories($this->categoryRepository->findBy(['id' => $categoryIds]));
        }
        if ($authorIds = $form->get('authors')->getData()) {
            $book->setAuthors($this->articleRepository->findBy(['id' => $authorIds]));
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($book);
        $em->flush();
        $this->addFlash('notice', 'Created successfully');
        return $this->redirectToRoute('app_library');

    }

    private function uploadFile($cover, Book $book, Request $request)
    {
        if (!$cover) {
            return;
        }
        try {
            $newFilename = $book->uploadFile(
                $cover,
                $this->slugger,
                $this->getParameter('covers_directory')
            );
            $book->setCover($newFilename);
        } catch (FileException $e) {
            $this->addFlash('error', 'Unable to save the file');
            return $this->redirect($request->getUri());
        }
    }

    /**
     * @Route("/update/{id}", name="book_update")
     */
    public function update(Request $request, Book $book): Response
    {
        $authors = $this->articleRepository->getAuthorsForSelect();
        $categories = $this->categoryRepository->getCategoriesForSelect();

        $form = $this->createForm(
            BookType::class,
            $book,
            ['authors' => $authors, 'categories' => $categories]
        );
        $form->get('categories')->setData($this->categoryRepository->getCategoriesForSelect($book->getId()));
        $form->get('authors')->setData($this->articleRepository->getAuthorsForSelect($book->getId()));
        $form->handleRequest($request);
        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('library/update.html.twig', [
                'form' => $form->createView(),
                'cover' => $book->getCover(),
                'id' => $book->getId()
            ]);
        }
        $cover = $form->get('cover')->getData();
        $this->uploadFile($cover, $book, $request);

        if ($categoryIds = $form->get('categories')->getData()) {
            $book->setCategories($this->categoryRepository->findBy(['id' => $categoryIds]));
        }
        if ($authorIds = $form->get('authors')->getData()) {
            $book->setAuthors($this->articleRepository->findBy(['id' => $authorIds]));
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($book);
        $em->flush();
        $this->addFlash('notice', 'Updated successfully');
        return $this->redirectToRoute('app_library');
    }

    /**
     * @Route("/delete/{id}", name="book_delete")
     */
    public function delete(Book $book): Response
    {
        $em = $this->getDoctrine()->getManager();
        $book->setIsDeleted(true);
        $em->persist($book);
        $em->flush();
        $this->addFlash('notice', 'Deleted successfully');
        return $this->redirectToRoute('app_library');
    }

    /**
     * @Route("/delete_completely/{id}", name="book_delete_completely")
     */
    public function deleteCompletely(Book $book): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($book);
        $em->flush();
        $this->addFlash('notice', 'Deleted successfully');
        return $this->redirectToRoute('app_library');
    }

    /**
     * @Route("/show/{id}", name="book_show")
     */
    public function show(Book $book): Response
    {
        $authors = [];
        foreach ($book->getAuthorsWithDescription() as $author) {
            $authors[] = $author['name'] . '<br>' . $author['description'];
        }
        $categories = [];
        foreach ($book->getCategoryWithSimilarBooks($this->bookRepository) as $category => $similarBooks) {
            $categories[$category] = implode(', ', $similarBooks);
        }
        return $this->render('library/show.html.twig', [
            'book' => $book,
            'authors' => $authors,
            'categories' => $categories
        ]);
    }
}
