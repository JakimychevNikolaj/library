<?php

namespace App\Controller\Library;

use App\Entity\Book;
use App\Form\BookType;
use App\Repository\AuthorRepository;
use App\Repository\BookRepository;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

class BookApiController extends AbstractApiController
{
    private BookRepository $bookRepository;
    private AuthorRepository $authorRepository;
    private CategoryRepository $categoryRepository;
    private SluggerInterface $slugger;

    public function __construct(
        BookRepository $bookRepository,
        AuthorRepository $authorRepository,
        CategoryRepository $categoryRepository,
        SluggerInterface $slugger
    )
    {
        $this->bookRepository = $bookRepository;
        $this->authorRepository = $authorRepository;
        $this->categoryRepository = $categoryRepository;
        $this->slugger = $slugger;
    }

    /**
     * @Route("/api/get_books", methods={"GET"}, name="get_books")
     */
    public function getBooks(Request $request): Response
    {
        return $this->respond($this->bookRepository->getBookList());
    }

    /**
     * @Route("/api/get_book/{id}", methods={"GET"}, name="get_book")
     */
    public function getBook(Request $request, Book $book): Response
    {
        if ($book->getIsDeleted()) {
            return $this->respond(['error' => 'This book is deleted'], 403);
        }
        return $this->respond($book->getDataFull($this->bookRepository));
    }

    /**
     * @Route("/api/create_book", methods={"POST"}, name="create_book")
     */
    public function createBook(Request $request): Response
    {
        $authors = $this->authorRepository->getAuthorsForSelect();
        $categories = $this->categoryRepository->getCategoriesForSelect();
        $form = $this->buildForm(
        BookType::class,
            null,
            ['authors' => $authors, 'categories' => $categories]
        );
        $form->handleRequest($request);
        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->respond($form, Response::HTTP_BAD_REQUEST);
        }
        $book = $form->getData();
        if ($categoryIds = $form->get('categories')->getData()) {
           $book->setCategories($this->categoryRepository->findBy(['id' => $categoryIds]));
        }
        if ($authorIds = $form->get('authors')->getData()) {
            $book->setAuthors($this->authorRepository->findBy(['id' => $authorIds]));
        }
        $book->setCreated(time());
        $this->getDoctrine()->getManager()->persist($book);
        $this->getDoctrine()->getManager()->flush();
        return $this->respond($book->getDataFull($this->bookRepository));
    }

    /**
     * @Route("/api/update_book/{id}", methods={"PATCH"}, name="update_book")
     */
    public function updateBook(Request $request, Book $book): Response
    {
        $authors = $this->authorRepository->getAuthorsForSelect();
        $categories = $this->categoryRepository->getCategoriesForSelect();
        $form = $this->buildForm(
            BookType::class,
            $book,
            [
                'authors' => $authors,
                'categories' => $categories,
                'method' => $request->getMethod()
            ]
        );
        $form->handleRequest($request);
        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->respond($form, Response::HTTP_BAD_REQUEST);
        }
        $book = $form->getData();
        if ($categoryIds = $form->get('categories')->getData()) {
            $book->setCategories($this->categoryRepository->findBy(['id' => $categoryIds]));
        }
        if ($authorIds = $form->get('authors')->getData()) {
            $book->setAuthors($this->authorRepository->findBy(['id' => $authorIds]));
        }
        $this->getDoctrine()->getManager()->persist($book);
        $this->getDoctrine()->getManager()->flush();
        return $this->respond($book->getDataFull($this->bookRepository));
    }

    /**
     * @Route("/api/delete_book/{id}", methods={"DELETE"}, name="delete_book")
     */
    public function deleteBook(Request $request, Book $book): Response
    {
        if (!$book || $book->getIsDeleted()) {
            return $this->respond(['error' => 'Book not found'], 404);
        }

        $em = $this->getDoctrine()->getManager();
        $book->setIsDeleted(true);
        $em->persist($book);
        $em->flush();
        return $this->respond(['message' => 'success']);
    }
}
