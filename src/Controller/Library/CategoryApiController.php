<?php

namespace App\Controller\Library;

use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryApiController extends AbstractApiController
{
    private CategoryRepository $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @Route("/api/get_categories", methods={"GET"}, name="get_categories")
     */
    public function getCategories(Request $request): Response
    {
        return $this->respond($this->categoryRepository->getCategoryList());
    }
}
