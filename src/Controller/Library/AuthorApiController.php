<?php

namespace App\Controller\Library;

use App\Entity\Author;
use App\Repository\AuthorRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class AuthorApiController extends AbstractApiController
{
    private AuthorRepository $authorRepository;

    public function __construct(AuthorRepository $authorRepository)
    {
        $this->authorRepository = $authorRepository;
    }

    /**
     * @Route("/api/get_authors", methods={"GET"}, name="get_authors")
     */
    public function getAuthors(Request $request): Response
    {
        return $this->respond($this->authorRepository->getAuthorList());
    }

    /**
     * @Route("/api/get_author/{id}", methods={"GET"}, name="get_author")
     */
    public function getAuthor(Request $request, Author $author): Response
    {
        return $this->respond($author->getDataFull());
    }
}
