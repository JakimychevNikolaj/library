<?php

namespace App\Entity;

use App\Repository\BookRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @ORM\Entity(repositoryClass=BookRepository::class)
 */
class Book
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_deleted;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_bestseller;

    /**
     * @ORM\Column(type="integer")
     */
    private $publishing_year;

    /**
     * @ORM\Column(type="integer")
     */
    private $created;

    /**
     * @ORM\Column(type="string")
     */
    private $isbn;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="text")
     */
    private $excerpt;

    /**
     * @ORM\ManyToMany(targetEntity=Author::class, inversedBy="books")
     */
    private $authors;

    /**
     * @ORM\ManyToMany(targetEntity=Category::class, inversedBy="books")
     */
    private $categories;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cover;

    public function __construct()
    {
        $this->authors = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsDeleted(): ?bool
    {
        return $this->is_deleted;
    }

    public function setIsDeleted(bool $is_deleted): self
    {
        $this->is_deleted = $is_deleted;

        return $this;
    }

    public function getIsBestSellerIcon(): ?string
    {
        return $this->is_bestseller
            ? '<i class="bi bi-check bi-4x" style="font-size: 30px"></i>'
            : '<i class="bi bi-x bi-4x" style="font-size: 30px"></i>';
    }

    public function getIsBestSeller(): ?bool
    {
        return $this->is_bestseller;
    }

    public function setIsBestSeller(bool $is_bestseller): self
    {
        $this->is_bestseller = $is_bestseller;

        return $this;
    }

    public function getPublishingYear(): ?int
    {
        return $this->publishing_year;
    }

    public function setPublishingYear(int $publishing_year): self
    {
        $this->publishing_year = $publishing_year;

        return $this;
    }

    public function getCreated(): ?int {
        return $this->created;
    }

    public function getCreatedDate(): ?string {
        return date('Y-m-d H:i:s', $this->created);
    }

    public function setCreated($created): self {
        $this->created = $created;

        return $this;
    }

    public function getIsbn(): ?string {
        return $this->isbn;
    }

    public function setIsbn($isbn): self {
        $this->isbn = $isbn;

        return $this;
    }

    public function getDescription(): ?string {
        return $this->description;
    }

    public function setDescription($description): self {
        $this->description = $description;

        return $this;
    }

    public function getExcerpt(): ?string {
        return $this->excerpt;
    }

    public function setExcerpt($excerpt): self {
        $this->excerpt = $excerpt;

        return $this;
    }

    /**
     * @return Collection<int, Author>
     */
    public function getAuthors(): Collection
    {
        return $this->authors;
    }

    public function setAuthors($authors): self
    {
        $this->authors = $authors;
        return $this;
    }

    public function addAuthor(Author $author): self
    {
        if (!$this->authors->contains($author)) {
            $this->authors[] = $author;
        }

        return $this;
    }

    public function removeAuthor(Author $author): self
    {
        $this->authors->removeElement($author);

        return $this;
    }

    public function getAuthorsWithDescription(): array {
        $authors = [];
        foreach ($this->getAuthors() as $author) {
            $authors[] = ['name' => $author->getFullName(), 'description' => $author->getInfo()];
        }
        return $authors;
    }
    /**
     * @return string
     */
    public function getAuthorNames(): ?string {
        $authors = $this->getAuthors();
        $result = '';
        foreach ($authors as $author) {
            $result .= $author->getFullName() . ', ';
        }
        return rtrim($result, ', ');
    }

    /**
     * @return Collection<int, Category>
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function setCategories($categories): self
    {
        $this->categories = $categories;
        return $this;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        $this->categories->removeElement($category);

        return $this;
    }

    public function getCategoryNames(): ?string {
        $categories = $this->getCategories();
        $result = '';
        foreach ($categories as $category) {
            $result .= $category->getName() . ', ';
        }
        return rtrim($result, ', ');
    }

    public function getCategoryWithSimilarBooks(BookRepository $bookRepository): array {
        $categories = [];
        foreach ($this->getCategories() as $category) {
            $bookNames = [];
            foreach ($bookRepository->findByCategory($category->getId(), $this->getId()) as $book) {
                $bookNames[] = $book->getName();
            }
            $categories[$category->getName()] = $bookNames;
        }
        return $categories;
    }

    public function uploadFile(UploadedFile $file, SluggerInterface $slugger, string $dirName): ?string {
        $newFilename = null;
        if ($file) {
            $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $slugger->slug($originalFilename);
            $newFilename = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();
            $file->move(
                $dirName,
                $newFilename
            );

        }
        return $newFilename;
    }

    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function setCover(?string $cover): self
    {
        $this->cover = $cover;

        return $this;
    }

    public function getDataShort(): array {
        $authors = [];
        foreach ($this->getAuthors() as $author) {
            $authors[] = $author->getDataShort();
        }
        $categories = [];
        foreach ($this->getCategories() as $category) {
            $categories[] = $category->getDataShort();
        }
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'authors' => $authors,
            'categories' => $categories,
            'publishing_year' => $this->getPublishingYear(),
            'isbn' => $this->getIsbn(),
            'created_date' => $this->getCreatedDate(),
            'is_bestseller' => $this->getIsBestSeller(),
        ];
    }

    public function getDataFull(BookRepository $bookRepository): array {
        $authors = [];
        foreach ($this->getAuthors() as $author) {
            $authors[] = $author->getDataShort();
        }
        $categories = [];
        foreach ($this->getCategories() as $category) {
            $categories[] = $category->getDataShort();
        }
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'authors' => $authors,
            'categories' => $categories,
            'similar_books' => $this->getCategoryWithSimilarBooks($bookRepository),
            'is_deleted' => $this->getIsDeleted(),
            'publishing_year' => $this->getPublishingYear(),
            'is_bestseller' => $this->getIsBestSeller(),
            'created_date' => $this->getCreatedDate(),
            'isbn' => $this->getIsbn(),
            'description' => $this->getDescription(),
            'excerpt' => $this->getExcerpt(),
            'cover' => $this->getCover(),
        ];
    }
}
