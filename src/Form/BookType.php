<?php

namespace App\Form;

use App\Entity\Book;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotNull(),
                    new Length(['min' => 3])
                ],
            ])
            ->add('is_deleted')
            ->add('is_bestseller')
            ->add('publishing_year')
            ->add('isbn')
            ->add('authors', ChoiceType::class,
                [
                    'mapped' => false,
                    'multiple' => true,
                    'choices'  => $options['authors'],
                ]
            )
            ->add('categories', ChoiceType::class,
                [
                    'required' => false,
                    'mapped' => false,
                    'multiple' => true,
                    'choices'  => $options['categories'],
                ]
            )
            ->add('description', TextareaType::class, [
                'required' => true,
                'constraints' => [
                    new NotNull()
                ],
            ])
            ->add('excerpt')
            ->add('cover', FileType::class, [
                'label' => 'Cover',
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '32768k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid png/jpg image',
                    ])
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
            'authors' => [],
            'categories' => []
        ]);
    }
}
