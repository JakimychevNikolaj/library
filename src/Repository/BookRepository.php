<?php

namespace App\Repository;

use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Book $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @inheritDoc
     */
    public function findAll() {
        return $this->createQueryBuilder('b')
            ->andWhere('b.is_deleted = 0')
            ->orderBy('b.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Book $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }


    /**
     * @param int $categoryId
     * @param int $exceptBookId
     * @return Book[] Returns an array of Book objects
     */
    public function findByCategory($categoryId, $exceptBookId = null)
    {
        $conn = $this->getEntityManager()->getConnection();
        $query = "select id from book b inner join book_category bc on b.id=bc.book_id and bc.category_id=:category_id";
        $params = [':category_id' => $categoryId];
        if ($exceptBookId) {
            $query .= ' and bc.book_id<>:except_book_id';
            $params[':except_book_id'] = $exceptBookId;
        }
        $stmt = $conn->prepare($query);
        $ids = array_column($stmt->executeQuery($params)->fetchAll(), 'id');
        return $this->findBy(['id' => $ids]);
    }

    /**
     * @return Book[] Returns an array of Book objects
     */
    public function getBookList() {
        $result = [];
        foreach ($this->findAll() as $book) {
            $result[] = $book->getDataShort();
        }
        return $result;
    }

    // /**
    //  * @return Book[] Returns an array of Book objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Book
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
