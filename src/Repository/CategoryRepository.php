<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Category $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Category $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param int $bookId
     * @return array
     */
    public function getCategoriesForSelect($bookId = null)
    {
        $result = [];
        $categories = $bookId ? $this->findByBookId($bookId) : $this->findAll();
        foreach ($categories as $category) {
            $result[$category->getName()] = $category->getId();
        }
        return $result;
    }

     /**
      * @return Category[] Returns an array of Category objects
      */
    public function findByBookId($bookId)
    {
        $conn = $this->getEntityManager()->getConnection();
        $query = "select id from category c inner join book_category bc on c.id=bc.category_id and bc.book_id=:book_id";
        $stmt = $conn->prepare($query);
        $ids = array_column($stmt->executeQuery([':book_id' => $bookId])->fetchAll(), 'id');
        return $this->findBy(['id' => $ids]);
    }

    /**
     * @return Category[] Returns an array of Category objects
     */
    public function getCategoryList() {
        $result = [];
        foreach ($this->findAll() as $category) {
            $result[] = $category->getDataShort();
        }
        return $result;
    }

    /*
    public function findOneBySomeField($value): ?Category
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
