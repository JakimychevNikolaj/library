<?php

namespace App\Repository;

use App\Entity\Author;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Author|null find($id, $lockMode = null, $lockVersion = null)
 * @method Author|null findOneBy(array $criteria, array $orderBy = null)
 * @method Author[]    findAll()
 * @method Author[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Author::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Author $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Author $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param int $bookId
     * @return array
     */
    public function getAuthorsForSelect($bookId = null)
    {
        $result = [];
        $authors = $bookId ? $this->findByBookId($bookId) : $this->findAll();
        foreach ($authors as $author) {
            $result[$author->getFullName()] = $author->getId();
        }
        return $result;
    }

     /**
      * @return Author[] Returns an array of Author objects
      */
    public function findByBookId($bookId)
    {
        $conn = $this->getEntityManager()->getConnection();
        $query = "select id from author a inner join book_author ba on a.id=ba.author_id and ba.book_id=:book_id";
        $stmt = $conn->prepare($query);
        $ids = array_column($stmt->executeQuery([':book_id' => $bookId])->fetchAll(), 'id');
        return $this->findBy(['id' => $ids]);
    }

    /**
     * @return Author[] Returns an array of Author objects
     */
    public function getAuthorList() {
        $result = [];
        foreach ($this->findAll() as $author) {
            $result[] = $author->getDataShort();
        }
        return $result;
    }

    /*
    public function findOneBySomeField($value): ?Author
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
