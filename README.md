# How to run project (with docker)

On first run :

```bash
$ make install
```

On next runs :

```bash
$ make start
```

# ![Db](db.png)


# Api Documentation

## Books:

GET /api/get_books

GET /api/get_book/{id}

POST /api/create_book

* string name
* string description
* [ids] categories
* [ids] authors
* boolean is_bestseller
* string excerpt
* string isbn
* int publishing_year


PATCH /api/update_book/{id}

* string name
* string description
* [ids] categories
* [ids] authors
* boolean is_bestseller
* string excerpt
* string isbn
* int publishing_year

## Authors:

GET /api/get_authors

GET /api/get_author/{id}

## Categories:

GET /api/get_categories
