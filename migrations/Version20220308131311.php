<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220308131311 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE book (
                                id INT AUTO_INCREMENT NOT NULL, 
                                name VARCHAR(255) NOT NULL, 
                                is_deleted TINYINT(1) DEFAULT 0 NOT NULL, 
                                is_bestseller TINYINT(1) DEFAULT 0 NOT NULL, 
                                publishing_year SMALLINT, 
                                created INT DEFAULT(UNIX_TIMESTAMP()), 
                                isbn VARCHAR(255), 
                                description TEXT, 
                                excerpt TEXT, 
                                cover VARCHAR(255), 
                                PRIMARY KEY(id)
                             ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TRIGGER book_timestamp_trigger BEFORE INSERT ON book FOR EACH ROW SET new.created = UNIX_TIMESTAMP(NOW())');
        $this->addSql("INSERT INTO book (name, is_bestseller, publishing_year, isbn, description) VALUES ('How to Stop Worrying and Start Living', 1, 1948, '9780671035976', 'How to Stop Worrying and Start Living is a self-help book by Dale Carnegie. It was first printed in Great Britain in 1948 by Richard Clay, Ltd., Bungay Suffolk. It is currently published as a Mass Market Paperback of 352 pages by Pocket')");
        $this->addSql("INSERT INTO book (name, is_bestseller, publishing_year, isbn, description) VALUES ('Star Wars: From the Adventures of Luke Skywalker', 1, 1976, '9781569713631', 'Star Wars: From the Adventures of Luke Skywalker is the novelization of the 1977 film Star Wars, ghostwritten by Alan Dean Foster, but credited to George Lucas. It was first published on November 12, 1976, by Ballantine Books, several months before the release of the film. ')");
        $this->addSql("INSERT INTO book (name, is_bestseller, publishing_year, isbn, description) VALUES ('The Lord of the Rings', 1, 1954, '9788845292613', 'The Lord of the Rings is a series of three epic fantasy adventure films directed by Peter Jackson, based on the novel written by J. R. R. Tolkien. The films are subtitled The Fellowship of the Ring, The Two Towers, and The Return of the King.')");
        $this->addSql("INSERT INTO book (name, is_bestseller, publishing_year, isbn, description) VALUES ('Good Omens', 0, 1990, '9780425132159', 'Good Omens: The Nice and Accurate Prophecies of Agnes Nutter, Witch is a 1990 novel written as a collaboration between the English authors Terry Pratchett and Neil Gaiman. The book is a comedy about the birth of the son of Satan and the coming of the End Times.')");
        $this->addSql("INSERT INTO book (name, is_bestseller, publishing_year, isbn, description) VALUES ('Mort', 0, 1987, '9780552144292', 'Mort is a fantasy novel by British writer Terry Pratchett. Published in 1987, it is the fourth Discworld novel and the first to focus on the character Death, who only appeared as a side character in the previous novels.')");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE book');
    }
}
