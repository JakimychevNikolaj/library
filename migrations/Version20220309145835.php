<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220309145835 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE book_author (book_id INT NOT NULL, author_id INT NOT NULL, INDEX IDX_9478D34516A2B381 (book_id), INDEX IDX_9478D345F675F31B (author_id), PRIMARY KEY(book_id, author_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE book_author ADD CONSTRAINT FK_9478D34516A2B381 FOREIGN KEY (book_id) REFERENCES book (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE book_author ADD CONSTRAINT FK_9478D345F675F31B FOREIGN KEY (author_id) REFERENCES author (id) ON DELETE CASCADE');

        $this->addSql("INSERT INTO book_author (book_id, author_id) VALUES (1, 5)");
        $this->addSql("INSERT INTO book_author (book_id, author_id) VALUES (2, 4)");
        $this->addSql("INSERT INTO book_author (book_id, author_id) VALUES (3, 3)");
        $this->addSql("INSERT INTO book_author (book_id, author_id) VALUES (4, 1)");
        $this->addSql("INSERT INTO book_author (book_id, author_id) VALUES (4, 2)");
        $this->addSql("INSERT INTO book_author (book_id, author_id) VALUES (5, 2)");


        $this->addSql('CREATE TABLE book_category (book_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_1FB30F9816A2B381 (book_id), INDEX IDX_1FB30F9812469DE2 (category_id), PRIMARY KEY(book_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE book_category ADD CONSTRAINT FK_1FB30F9816A2B381 FOREIGN KEY (book_id) REFERENCES book (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE book_category ADD CONSTRAINT FK_1FB30F9812469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');

        $this->addSql("INSERT INTO book_category (book_id, category_id) VALUES (1, 3)");
        $this->addSql("INSERT INTO book_category (book_id, category_id) VALUES (2, 2)");
        $this->addSql("INSERT INTO book_category (book_id, category_id) VALUES (3, 5)");
        $this->addSql("INSERT INTO book_category (book_id, category_id) VALUES (4, 4)");
        $this->addSql("INSERT INTO book_category (book_id, category_id) VALUES (4, 5)");
        $this->addSql("INSERT INTO book_category (book_id, category_id) VALUES (4, 6)");
        $this->addSql("INSERT INTO book_category (book_id, category_id) VALUES (5, 5)");

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE book_author');
        $this->addSql('DROP TABLE book_category');
    }
}
